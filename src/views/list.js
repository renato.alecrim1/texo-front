import { Grid } from "@mui/material";
import MoviesTable from "../components/tables/movies";

export default function MoviesList() {
  return (
    <Grid id="container" container>
      <MoviesTable />
    </Grid>
  );
}
