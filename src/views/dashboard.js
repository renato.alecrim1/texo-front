import { Grid } from "@mui/material";
import MultipleWinnersTable from "../components/tables/multiple-winners";
import StudioWinnersTable from "../components/tables/studios-winners";
import ProducersWinInterval from "../components/tables/producers-win-interval";
import MoviesYearTable from "../components/tables/movies-year";

export default function Dashboard() {
  return (
    <Grid id="container" container spacing={2}>
      <Grid item xs={6}>
        <MultipleWinnersTable />
      </Grid>
      <Grid item xs={6}>
        <StudioWinnersTable />
      </Grid>
      <Grid item xs={6}>
        <ProducersWinInterval />
      </Grid>
      <Grid item xs={6}>
        <MoviesYearTable />
      </Grid>
    </Grid>
  );
}
