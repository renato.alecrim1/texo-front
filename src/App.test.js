import { fireEvent, render, screen } from "@testing-library/react";
import App from "./App";
import MoviesList from "./views/list";
import MoviesYearTable from "./components/tables/movies-year";
import Dashboard from "./views/dashboard";

test("renders sidebar and toolbar", () => {
  render(<App />);
  const dashboardElement = screen.getByText("Dashboard");
  const listElement = screen.getByText("List");
  const toolbarElement = screen.getByText("Golden Raspberry Awards");
  expect(dashboardElement).toBeInTheDocument();
  expect(listElement).toBeInTheDocument();
  expect(toolbarElement).toBeInTheDocument();
});

test("renders all tables on dashboard", () => {
  render(<Dashboard />);
  const multipleElement = screen.getByText("List years with multiple winners");
  const studioElement = screen.getByText("Top 3 studios with winners");
  const producersElement = screen.getByText(
    "Producers with longest and shortest interaval between wins"
  );
  const listElement = screen.getByText("List movie winners by year");

  expect(multipleElement).toBeInTheDocument();
  expect(studioElement).toBeInTheDocument();
  expect(producersElement).toBeInTheDocument();
  expect(listElement).toBeInTheDocument();
});

test("renders all tables on movies list", () => {
  render(<MoviesList />);
  const moviesElement = screen.getByText("List movies");
  const inputYearElement = screen.getByLabelText("Filter by year");
  const inputWinnerElement = screen.getByLabelText("Yes / No");
  const paginationElement = screen.getByTitle("Go to next page");

  expect(moviesElement).toBeInTheDocument();
  expect(inputYearElement).toBeInTheDocument();
  expect(inputWinnerElement).toBeInTheDocument();
  expect(paginationElement).toBeInTheDocument();
});

test("renders search input and pagination", () => {
  render(<MoviesYearTable />);
  const onClick = jest.fn();
  const inputElement = screen.getByLabelText("Search by year");
  const paginationElement = screen.getByTitle("Go to next page");
  const buttonElement = screen.getByTitle("Search");

  expect(inputElement).toBeInTheDocument();
  expect(paginationElement).toBeInTheDocument();
  expect(buttonElement).toBeInTheDocument();

  buttonElement.onclick = onClick;
  fireEvent.click(buttonElement);
  expect(onClick).toHaveBeenCalled();
});
