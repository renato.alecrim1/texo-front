import "./App.css";
import { ThemeProvider } from "@mui/material/styles";
import { theme } from "./theme/theme";
import RouterComponent from "./router/router";
import React from "react";

function App() {
  const [isShown] = React.useState(true);
  return (
    <ThemeProvider theme={theme}>
      {isShown ? <RouterComponent /> : <div />}
    </ThemeProvider>
  );
}

export default App;
