const apiUri = "https://tools.texoit.com/backend-java/api/movies";

const MoviesService = {
  /**
   * Returns a list of years with more than one winner movie.
   */
  getYearsMultipleWinners: async () => {
    try {
      const response = await fetch(
        `${apiUri}?projection=years-with-multiple-winners`,
        { method: "GET" }
      );
      const movies = await response.json();

      return movies?.years || [];
    } catch (error) {
      console.log(error);
      return [];
    }
  },
  /**
   * Returns a list of studios and they win count.
   */
  getStudios: async () => {
    try {
      const response = await fetch(
        `${apiUri}?projection=studios-with-win-count`,
        { method: "GET" }
      );
      const studios = await response.json();

      return studios?.studios || [];
    } catch (error) {
      console.log(error);
      return [];
    }
  },
  /**
   * Returns an object of the producers with the min and max yearns between wins.
   */
  getProducersInterval: async () => {
    try {
      const response = await fetch(
        `${apiUri}?projection=max-min-win-interval-for-producers`,
        { method: "GET" }
      );
      const producers = await response.json();
      return producers || {};
    } catch (error) {
      console.log(error);
      return {};
    }
  },
  /**
   * Returns an object with a content list of movies by year and pagination.
   */
  getMoviesByYear: async ({ year, winner, page, rows = 20 }) => {
    try {
      const response = await fetch(
        `${apiUri}?page=${page}&size=${rows}${
          winner !== undefined ? `&winner=${winner}` : ""
        }${!!year ? `&year=${year}` : ""}`,
        { method: "GET" }
      );
      const producers = await response.json();
      return producers || {};
    } catch (error) {
      console.log(error);
      return {};
    }
  },
};

export default MoviesService;
