import { createTheme } from "@mui/material";

export const theme = createTheme({
  // Sizes & Spacing
  sidebarWidth: "250px",
  toolbarHeight: '40px',
  borderRadius: "6px",
  zIndex: {
    sidebar: 90,
    toolbar: 95,
  },

  components: {
    MuiGrid: {
      variants: [
        {
          props: { id: "container" },
          style: {
            padding: '16px',
          },
        },
      ],
    },
    MuiTypography: {
      variants: [
        {
          props: { id: "table-title" },
          style: {
            fontWeight: "700",
            marginBottom: '8px'
          },
        },
      ],
    },
    MuiIconButton: {
      variants: [
        {
          props: { id: "icon-primary" },
          style: {
            backgroundColor: "#007ad9",
            color: "#fff",
            borderRadius: "6px",
          },
        },
      ],
    },
    MuiTableCell: {
      variants: [
        {
          props: { id: "header" },
          style: {
            cursor: "default",
            fontWeight: "600",
            backgroundColor: "#f4f4f4",
            whiteSpace: "nowrap",
          },
        },
      ],
    },
  },

  // Colors
  palette: {
    sidebarColor: "#f8f9fb",
    toolbarColor: "#353a40",
    white: "#fff",
  },
});

export const getSidebarWidth = () => {
  const isOpen = window.localStorage.getItem("isSidebarOpen");
  if (isOpen === null || isOpen === "true") {
    return theme.sidebarWidthOpen;
  }

  return theme.sidebarWidth;
};
