import styled from "@emotion/styled";
import { useLocation, NavLink } from "react-router-dom";
import {
  Drawer,
  List,
  ListItemButton,
  ListItem,
  ListItemText,
} from "@mui/material";
import { DASHBOARD, MOVIES_LIST } from "../constants/routes";

const Sidebar = () => {
  const { pathname } = useLocation();

  const checkArialCurrent = (path) => {
    if (path === pathname) return "true";
    return null;
  };

  return (
    <DrawerStyled variant="permanent" anchor="left">
      <List sx={styles.list}>
        <ListItem
          component={NavLink}
          to={DASHBOARD}
          aria-current={checkArialCurrent("/dashboard")}
        >
          <ListItemButton>
            <ListItemText
              sx={pathname === "/dashboard" ? styles.activeMenu : styles.menu}
              primary="Dashboard"
            />
          </ListItemButton>
        </ListItem>
        <ListItem
          component={NavLink}
          to={MOVIES_LIST}
          aria-current={checkArialCurrent("/list")}
        >
          <ListItemButton>
            <ListItemText
              sx={pathname === "/list" ? styles.activeMenu : styles.menu}
              primary="List"
            />
          </ListItemButton>
        </ListItem>
      </List>
    </DrawerStyled>
  );
};

const styles = {
  menu: {
    color: "#98999d",
  },
  activeMenu: {
    color: "#168af8",
  },
  list: {
    marginTop: (theme) => theme.toolbarHeight,
  },
};

const DrawerStyled = styled(Drawer)(({ theme }) => {
  return {
    position: "relative",
    zIndex: theme.zIndex.sidebar,
    "& .MuiDrawer-paper": {
      width: theme.sidebarWidth,
      display: "flex",
      height: "100vh",
      overflowX: "hidden",
      background: theme.palette.sidebarColor,
      flexDirection: "column",
      justifyContent: "space-between",
      zIndex: 7,
      border: "none",
    },
  };
});

export default Sidebar;
