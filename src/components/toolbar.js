import React from "react";
import { AppBar, Typography } from "@mui/material";

export default function Toolbar() {
  return (
    <AppBar sx={styles.appBar} position="relative">
      <Typography sx={styles.title}>Golden Raspberry Awards</Typography>
    </AppBar>
  );
}

const styles = {
  appBar: {
    display: "flex",
    justifyContent: "center",
    padding: "0 16px",
    background: (theme) => `${theme.palette.toolbarColor} !important`,
    width: "100% !important",
    height: (theme) => `${theme.toolbarHeight} !important`,
    zIndex: (theme) => theme.zIndex.toolbar,
    boxShadow: "none",
  },
  title: {
    color: "#fff",
  },
};
