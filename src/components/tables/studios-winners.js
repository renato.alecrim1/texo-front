import {
  Grid,
  Typography,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
  Paper,
  TableContainer,
  Box,
} from "@mui/material";
import { useEffect, useState } from "react";
import MoviesService from "../../services/movies-service";

const columns = [
  { id: "name", label: "Name" },
  { id: "winCount", label: "Win count" },
];

const StudioWinnersTable = () => {
  const [studios, setStudios] = useState([]);

  useEffect(() => {
    MoviesService.getStudios().then((studios) =>
      setStudios(studios.slice(0, 3))
    );
  }, []);

  return (
    <Grid component={Paper} elevation={2} container padding={2}>
      <Typography id="table-title">Top 3 studios with winners</Typography>
      <TableContainer component={Paper}>
        <Box overflow="auto">
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell id="header" key={column.id}>{column.label}</TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {studios.map((studio, index) => (
                <TableRow hover role="checkbox" tabIndex={-1} key={index}>
                  {columns.map((column) => {
                    return (
                      <TableCell key={column.id}>{studio[column.id]}</TableCell>
                    );
                  })}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </TableContainer>
    </Grid>
  );
};

export default StudioWinnersTable;
