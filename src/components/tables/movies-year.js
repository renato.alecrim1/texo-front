import {
  Grid,
  Typography,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
  Paper,
  TableContainer,
  Box,
  IconButton,
  TextField,
  TablePagination,
} from "@mui/material";
import { useEffect, useState } from "react";
import MoviesService from "../../services/movies-service";
import { Search } from "@mui/icons-material";

const columns = [
  { id: "id", label: "Id" },
  { id: "year", label: "Year" },
  { id: "title", label: "Title" },
];

const MoviesYearTable = () => {
  const [movies, setMovies] = useState({});
  const [page, setPage] = useState(0);
  const [year, setYear] = useState(undefined);
  const [rows] = useState(10);

  useEffect(() => {
    if (!year) return;
    handleSearch();

    // eslint-disable-next-line
  }, [page]);

  const handleSearch = () => {
    if (!year) return;
    MoviesService.getMoviesByYear({ year, page, rows }).then((result) =>
      setMovies(result || {})
    );
  };

  const onPageChange = (_, newPage) => {
    setPage(newPage);
  };

  const handleKeyUp = (event) => {
    if (event.keyCode === 13) {
      handleSearch();
    }
  };

  return (
    <Grid
      component={Paper}
      elevation={2}
      container
      padding={2}
      sx={styles.container}
    >
      <Typography id="table-title">List movie winners by year</Typography>
      <Grid container item xs={12} spacing={2} padding={"8px 0"}>
        <Grid item flexGrow={1}>
          <TextField
            value={year}
            label="Search by year"
            variant="outlined"
            size="small"
            type="number"
            fullWidth
            onKeyUp={handleKeyUp}
            onChange={({ target }) => setYear(target.value)}
          />
        </Grid>
        <Grid item>
          <IconButton id="icon-primary" title="Search" onClick={handleSearch}>
            <Search fontSize="inherit" />
          </IconButton>
        </Grid>
      </Grid>
      <TableContainer component={Paper}>
        <Box overflow="auto">
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell id="header" key={column.id}>{column.label}</TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {movies?.content?.map((movie, index) => (
                <TableRow hover role="checkbox" tabIndex={-1} key={index}>
                  {columns.map((column) => {
                    return (
                      <TableCell key={column.id}>{movie[column.id]}</TableCell>
                    );
                  })}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
        <TablePagination
          component="div"
          count={movies.totalElements || 0}
          rowsPerPageOptions={[]}
          rowsPerPage={rows}
          page={page}
          backIconButtonProps={{
            "aria-label": "previous page",
          }}
          nextIconButtonProps={{
            "aria-label": "next page",
          }}
          labelDisplayedRows={({ from, to, count }) =>
            `${from}-${to} de ${count}`
          }
          onPageChange={onPageChange}
        />
      </TableContainer>
    </Grid>
  );
};

const styles = {
  container: {
    height: "100%",
    alignContent: "flex-start",
  },
};

export default MoviesYearTable;
