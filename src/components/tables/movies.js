import {
  Grid,
  Typography,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
  Paper,
  TableContainer,
  Box,
  TextField,
  MenuItem,
  TablePagination,
} from "@mui/material";
import { useEffect, useState } from "react";
import MoviesService from "../../services/movies-service";

const columns = [
  { id: "id", label: "ID" },
  { id: "year", label: "Year" },
  { id: "title", label: "Title" },
  {
    id: "winner",
    label: "Winner?",
    format: (winner) => (winner ? "Yes" : "No"),
  },
];

const MoviesTable = () => {
  const [movies, setMovies] = useState({});
  const [page, setPage] = useState(0);
  const [year, setYear] = useState("");
  const [winner, setWinner] = useState("");
  const [rows] = useState(10);

  useEffect(() => {
    handleSearch();

    // eslint-disable-next-line
  }, [page, winner, year]);

  useEffect(() => {
    setPage(0);

    // eslint-disable-next-line
  }, [winner, year]);

  const handleSearch = () => {
    MoviesService.getMoviesByYear({ year, winner, page, rows }).then(
      (result) => {
        setMovies(result || {});
      }
    );
  };

  const onPageChange = (_, newPage) => {
    setPage(newPage);
  };

  return (
    <Grid
      component={Paper}
      elevation={2}
      container
      padding={2}
      sx={styles.container}
    >
      <Typography id="table-title">List movies</Typography>
      <TableContainer component={Paper}>
        <Box overflow="auto">
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                <TableCell id="header" key={"id"} sx={styles.header}>
                  ID
                </TableCell>
                <TableCell id="header" key={"year"} sx={styles.header}>
                  <Grid container direction="column" alignItems="center">
                    Year
                    <TextField
                      value={year}
                      label="Filter by year"
                      variant="outlined"
                      size="small"
                      type="number"
                      fullWidth
                      onChange={({ target }) => setYear(target.value)}
                    />
                  </Grid>
                </TableCell>
                <TableCell id="header" key={"title"} sx={styles.header}>
                  Title
                </TableCell>
                <TableCell id="header" key={"winner"} sx={styles.header}>
                  <Grid container direction="column" alignItems="center">
                    Winner?
                    <TextField
                      select
                      value={winner}
                      label="Yes / No"
                      variant="outlined"
                      size="small"
                      type="number"
                      fullWidth
                      onChange={({ target }) => setWinner(target.value)}
                    >
                      <MenuItem key={"all"} value={undefined}>
                        All
                      </MenuItem>
                      <MenuItem key={"yes"} value={true}>
                        Yes
                      </MenuItem>
                      <MenuItem key={"no"} value={false}>
                        No
                      </MenuItem>
                    </TextField>
                  </Grid>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {movies?.content?.map((movie) => {
                console.log(movie);
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={movie.id}>
                    {columns.map((column) => {
                      return (
                        <TableCell key={column.id}>
                          {column.format
                            ? column.format(movie[column.id])
                            : movie[column.id]}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
        <TablePagination
          component="div"
          count={movies.totalElements || 0}
          rowsPerPageOptions={[]}
          rowsPerPage={rows}
          page={page}
          backIconButtonProps={{
            "aria-label": "previous page",
          }}
          nextIconButtonProps={{
            "aria-label": "next page",
          }}
          labelDisplayedRows={({ from, to, count }) =>
            `${from}-${to} de ${count}`
          }
          onPageChange={onPageChange}
        />
      </TableContainer>
    </Grid>
  );
};

const styles = {
  container: {
    height: "100%",
    alignContent: "flex-start",
  },
  header: {
    width: "25%",
  },
};

export default MoviesTable;
