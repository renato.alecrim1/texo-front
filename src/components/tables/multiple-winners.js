import {
  Grid,
  Typography,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
  Paper,
  TableContainer,
  Box,
} from "@mui/material";
import { useEffect, useState } from "react";
import MoviesService from "../../services/movies-service";

const columns = [
  { id: "year", label: "Year" },
  { id: "winnerCount", label: "Win count" },
];

const MultipleWinnersTable = () => {
  const [multipleWinners, setMultiplWinners] = useState([]);

  useEffect(() => {
    MoviesService.getYearsMultipleWinners().then(setMultiplWinners);
  }, []);

  return (
    <Grid component={Paper} elevation={2} container padding={2}>
      <Typography id="table-title">List years with multiple winners</Typography>
      <TableContainer component={Paper}>
        <Box overflow="auto">
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell id="header" key={column.id}>{column.label}</TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {multipleWinners.map((winners) => (
                <TableRow
                  hover
                  role="checkbox"
                  tabIndex={-1}
                  key={winners.year}
                >
                  {columns.map((column) => {
                    return (
                      <TableCell key={column.id}>
                        {winners[column.id]}
                      </TableCell>
                    );
                  })}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </TableContainer>
    </Grid>
  );
};

export default MultipleWinnersTable;
