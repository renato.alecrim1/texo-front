import {
  Grid,
  Typography,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
  Paper,
  TableContainer,
  Box,
} from "@mui/material";
import { useEffect, useState } from "react";
import MoviesService from "../../services/movies-service";

const columns = [
  { id: "producer", label: "Producer" },
  { id: "interval", label: "Interval", width: "100px" },
  { id: "previousWin", label: "Previous Year", width: "100px" },
  { id: "followingWin", label: "Following Year", width: "100px" },
];

const ProducersWinInterval = () => {
  const [producersInterval, setProducersInterval] = useState([]);

  useEffect(() => {
    MoviesService.getProducersInterval().then(setProducersInterval);
  }, []);

  return (
    <Grid component={Paper} elevation={2} container padding={2}>
      <Typography id="table-title">
        Producers with longest and shortest interaval between wins
      </Typography>
      <Grid item xs={12}>
        <Typography marginBottom={"8px"}>Maximum</Typography>
        <TableContainer component={Paper}>
          <Box overflow="auto">
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  {columns.map((column) => (
                    <TableCell id="header" key={column.id} width={column.width}>
                      {column.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {producersInterval?.max?.map((producer, index) => (
                  <TableRow hover role="checkbox" tabIndex={-1} key={index}>
                    {columns.map((column) => {
                      return (
                        <TableCell key={column.id}>
                          {producer[column.id]}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Box>
        </TableContainer>
      </Grid>
      <Grid item xs={12}>
        <Typography margin={"8px 0"}>Minimum</Typography>
        <TableContainer component={Paper}>
          <Box overflow="auto">
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  {columns.map((column) => (
                    <TableCell id="header" key={column.id} width={column.width}>
                      {column.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {producersInterval?.min?.map((producer, index) => (
                  <TableRow hover role="checkbox" tabIndex={-1} key={index}>
                    {columns.map((column) => {
                      return (
                        <TableCell key={column.id}>
                          {producer[column.id]}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Box>
        </TableContainer>
      </Grid>
    </Grid>
  );
};

export default ProducersWinInterval;
