import { HashRouter, Route, Routes, Navigate } from "react-router-dom";
import Sidebar from "../components/sidebar";
import Toolbar from "../components/toolbar";
import { DASHBOARD, MOVIES_LIST } from "../constants/routes";
import Dashboard from "../views/dashboard";
import MoviesList from "../views/list";
import styled from "@emotion/styled";

export default function RouterComponent() {
  return (
    <HashRouter>
      <Toolbar />
      <FullWrapper>
        <Sidebar />
        <Content>
          <Main>
            <Routes>
              <Route path={DASHBOARD} element={<Dashboard />} />
              <Route path={MOVIES_LIST} element={<MoviesList />} />
              <Route path="*" element={<Navigate to={DASHBOARD} />} />
            </Routes>
          </Main>
        </Content>
      </FullWrapper>
    </HashRouter>
  );
}

const Content = styled.div`
  display: inline-block;
  width: 100%;
  transition: padding-left 0.2s ease-in-out;
  padding-left: ${({ theme }) => theme.sidebarWidth};
`;

const FullWrapper = styled.div`
  display: flex;
  position: relative;
  flex-direction: row;
  width: 100%;
  height: 100%;
  background: ${(props) => props.theme.palette.background};
  height: ${(props) => `calc(100vh - ${props.theme.toolbarHeight})`};
  padding
`;

const Main = styled.main`
  width: 100%;
  padding: 0;
  overflow-y: auto;
`;
